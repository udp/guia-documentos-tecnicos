# Introducción
El documento [guia-documentos-tecnicos.tex](guia-documentos-tecnicos.tex) es una guía con distintos lineamientos sobre la escritura de documentos técnicos.

- Introduce los componentes principales para la creación de documentos.
- Explica guías básicas sobre el uso de LaTeX y de distintos ambientes.
- Explica errores comunes de escritura y ortografía.

El documento [plantilla.tex](plantilla.tex) es un ejemplo de como utilizar LaTeX. Este es más compacto y más fácil de cambiar que el anterior. Por lo que se recomienda el uso de éste para iniciar sus documentos.